PKGNAME ?= btrfs-timeshift-apt-snapshots

.PHONY: install

install:
	@install -Dm644 -t "$(DESTDIR)/etc/apt/apt.conf.d/" 80-btrfs-timeshift-apt-snapshots
	@install -Dm755 -t "$(DESTDIR)/usr/bin/" btrfs-timeshift-apt-snapshots
	@install -Dm644 -t "$(LIB_DIR)/etc/" btrfs-timeshift-apt-snapshots.conf

uninstall:
	rm -f $(DESTDIR)/etc/apt/apt.conf.d/80-btrfs-timeshift-apt-snapshots
	rm -f $(DESTDIR)/usr/bin/btrfs-timeshift-apt-snapshots
	rm -f $(LIB_DIR)/etc/btrfs-timeshift-apt-snapshots.conf
